# kafka常用命令
---
---
## 1. 启动kafka：
启动kafka需要先启动zookeeper，这里可以使用自己的起的zookeeper

启动kafka：
```
bin/kafka-server-start.sh -deamon config/server.properties
```

查看topic
```
bin/kafka-topics.sh --zookeeper 127.0.0.1:2181 --list
```

创建topic
```
bin/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --replication-factor 1 --partitions 1 --topic first
```

删除topic
```
bin/kafka-topics.sh --zookeeper 127.0.0.1:2181 --delete --topic first
```

注意，副本数量不能够超过当前的kafka broker集群的数量

