## BGP学习

---
---

### 概念：
看这篇就可以：https://blog.csdn.net/le616616/article/details/121564799


### BGP学习内容

##### 基础

- BGP邻居建立
- BGP通告原则
- BGP路径属性
- BGP路由选择
- BGP路由汇聚
- 路由反射器和联盟
- MP-BGP

##### 进阶

- BGP-RR
- BGP-HA

## 0. 前置知识点
- CIDR：https://blog.csdn.net/chenyukk/article/details/107627724
- VLSM
- 路由震荡


## 1. BGP分类和区别

#### 分类

- EBGP：运行于不同AS之间的BGP称为EBGP，为了防止AS间产生环路，当BGP设备接收EBGP对等体发送的路由时，会将带有本地AS号的路由丢弃

- IBGP：运行于同一AS内部的BGP称为IBGP，为了防止AS内产生环路，BGP设备不将从IBGP对等体学到的路由通告给其他IBGP对等体，并与所有IBGP对等体建立全连接，为了解决IBGP对等体的连接数量太多的问题，BGP设计了路由反射器和BGP联盟

#### 区别

- EBGP在不同的AS号之间建立BGP关系，而IBGP与相同的AS号建立BGP关系
- 从IBGP对等体获得的BGP路由，BGP设备只发布给它的EBGP对等体
- 从EBGP对等体获得的BGP路由，BGP设备发布给它所有EBGP和IBGP对等体

#### 报文类型

- OPEN报文，协商BGP参数
- Update报文，交换路由信息
- keepalive报文，保持邻居关系
- notification报文，差错通知
- route-refresh报文，用于在改变路由策略之后请求对等体重新发送路由信息

## 2. BGP邻居是怎么建立的

![enter description here](./images/1658291404540.png)


- （稳定）idle：起始状态
- （稳定）connect：配置完BGP之后，就是connect状态，开始三次握手，可能的原因是三次握手的包一直没发出去
- （稳定）active：三次握手失败超时重传之后会进入重传状态，可能的原因
	- 没有回程路由
	- 中间设备防火墙179没放开
- opensent：如果三次握手建立连接之后进入
- openconfirm：
- （稳定）established：准备好的状态

## 3. BGP对等体之间交互的原则

BGP本身是没有路由的，需要从别的路由协议获取路由：
- 静态路由
- ospf等

1. 原则1：只发布最优路由
2. 原则2：IBGP peer通告的路由不会通告给其他的IBGP peer
3. 原则3：EBGP的peer的通告的路由会通告给其他IBGP peer，ibgp是不会改变吓一跳的但是net-hop不会改变

这样就会有两个问题：

1. 问题1：来自ebgp peer的路由通告，因为不IBGP不会改变not-hop的，所以来自ebgp的路由通告给其他ibgp peer的时候，路由的net-hop是源AS的边缘路由器，而这个对于目的AS来说是不可达的

解决办法是：
- （1）方法1（不推荐）：将R1的地址通过路由协议传到R6，R7
- （2）方法2：在本AS边缘路由器连接其他peer时，设置next-hop-self（华为是net-hop-local），也就是修改IBGP的默认行为，让IBGP在传递路由的时候，将路由的next-hop改为自己

:2. 问题2：因为IBGP peer通告的路由不会通告给其他的IBGP peer，主要是为了防止环路，解决这个问题的办法：

- 在AS内建立full mesh的邻居关系
- 使用bgp RR
- 使用BGP联盟


###### 什么是BGP RR

一些名词

- client
- non-client
- reflect-route


命令是
```
peer 1.1.1.1 reflect-client
```

这样来自1.1.1.1的这个peer的路由就会被反射到除了他以外的所有的client以及non-client

但是这种方式还是有问题，即next-hop还是不会变化

一般这个地方是部署OSPF的多

当然BGP-RR还有cluster-id和origin-id来防环


###### 什么是BGP联盟

BGP联盟用的比较少

将一个大的AS里面的BGP router，划分到多个小的sub-AS。通过这样的划分，减少IBGP peer连接数。例如，6个BGP router的full-mesh连接数是15。通过划分sub-AS可以减少到8个

参考：https://zhuanlan.zhihu.com/p/31766603?from_voters_page=true

但是BGP联盟配起来比较复杂一些，需要每一台设备都进行配置，因此用的较少。


## 3. BGP数据库

BGP有一些数据库：
- IP路由表：全局路由信息库，包括所有的IP路由信息
- BGP路由表：BGP路由信息库，包括本地BGP speaker选择的路由信息
- 邻居表：对等体邻居清单列表
- Adj-RIB-In：对等体宣告给本地BGP speaker的未处理的路由信息库
- Adj-RIB-Out：本地BGP speaker宣告给指定对等体的路由信息库
- 策略引擎：包括输入输出策略引擎，用来过滤对等体更新的信息
	- 输入策略
	- 输出策略：一个应用场景是，比如AS号是300，但是这个路径带宽比较低，所以可以通过添加AS为(300, 300, 300)这样来控制路由选路

![enter description here](./images/1658308249503.png)

## 4. BGP路由的属性特点

![enter description here](./images/1658308520617.png)

![enter description here](./images/1658309435803.png)

- 优先级：i > e > ?

![enter description here](./images/1658309829079.png)

![enter description here](./images/1658310488890.png)

![enter description here](./images/1658313680880.png)

![enter description here](./images/1658313899396.png)

- local-pref是越大越优先，这个是AS内部的，出AS的时候，优先选大的
- MED是越小越优先，AS之间的，确切的说是入AS的时候的，优先选最小的

![enter description here](./images/1658314620385.png)